# BIF sprite parser

Project based on code 
https://gist.github.com/pkulak/61de475a74d824a9d875

and some info from 
https://stackoverflow.com/questions/1557071/determining-the-size-of-a-jpeg-jfif-image


BIF file specification 
https://developer.roku.com/en-gb/docs/developer-program/media-playback/trick-mode/bif-file-creation.md#bif-file-specification
